Name:         perl-MIME-Base64
Version:      3.16
Release:      3
Summary:      Encoding and decoding of base64 strings
License:      (GPL-1.0-or-later OR Artistic-1.0-Perl) and MIT
URL:          https://metacpan.org/release/MIME-Base64
Source0:      https://cpan.metacpan.org/authors/id/G/GA/GAAS/MIME-Base64-%{version}.tar.gz

BuildRequires:perl-generators perl-interpreter perl-devel
BuildRequires:perl(ExtUtils::MakeMaker) >= 6.76 perl(Test) findutils make gcc

Conflicts:    perl < 4:5.22.0-347

%description
This module provides functions to encode and decode strings into and from the base64 encoding
specified in RFC 2045 - MIME (Multipurpose Internet Mail Extensions). The base64 encoding is
designed to represent arbitrary sequences of octets in a form that need not be humanly readable.
A 65-character subset ([A-Za-z0-9+/=]) of US-ASCII is used, enabling 6 bits to be represented per printable character.

%package_help

%prep
%autosetup -n MIME-Base64-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc README
%{perl_vendorarch}/*

%files help
%doc Changes
%{_mandir}/*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 3.16-3
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Oct 25 2022 wangjiang <wangjiang37@h-partners.com> - 3.16-2
- Rebuild for next release

* Sat Dec 25 2021 tianwei <tianwei12@huawei.com> - 3.16-1
- upgrade version to 3.16

* Wed Jun 23 2021 liudabo <liudabo1@huawei.com> - 3.15-420
- Add gcc build dependcy

* Wed May 13 2020 licunlong <licunlong1@huawei.com> - 3.15-419
- add perl-devel buildrequire

* Thu Sep 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.15-418 
- Package init
